Event Log File
==============

This module records events for private and temporary files using [Event Log][1].

Currently the following events are recorded:

- Download a private or temporary file.

Installation
------------

Use the standard Drupal module installation procedure.

Once installed, all every viewing of a private or temporary file will be
recorded in the event log at `admin/reports/events`.

[1]: https://drupal.org/project/event_log
